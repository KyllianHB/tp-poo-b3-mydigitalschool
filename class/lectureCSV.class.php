<?php

class lectureCSV
{
    private string $csvFile;

    public function __construct($csvFile)
    {
        $this->csvFile = $csvFile;
    }

    public function csvArray()
    {
        if (($open = fopen($this->csvFile, "r")) !== FALSE) 
        {
            while (($data = fgetcsv($open, 1000, ",")) !== FALSE) 
            {        
                $array[] = $data; 
            }
            fclose($open);
            return $array;
        }
    }
}
