<?php 

class CalculImpot
{

    public function __construct()
    {

    }

    public function getImpotRevenu($regimeFiscal, $regimeActivite, $CA_HT)
    {
        $abbatementForfaitaire = 0;

        if($regimeFiscal == 'Prélèvement à la source') 
        {

            switch ($regimeActivite) {
                case 'BNC':
                    $abbatementForfaitaire = 34;
                    break;                
                case 'BIC':
                    $abbatementForfaitaire = 50;
                    break;
                case 'BIC Vente':
                    $abbatementForfaitaire = 71;
                    break;
                default:
                    # code...
                    break;
            }        

            return $CA_HT * (1 - $abbatementForfaitaire/100);
        } 
        else if($regimeFiscal == 'Prélèvement libératoire') 
        {
            switch ($regimeActivite) {
                case 'BNC':
                    $abbatementForfaitaire = 2.2;
                    break;                
                case 'BIC':
                    $abbatementForfaitaire = 1.7;
                    break;
                case 'BIC Vente':
                    $abbatementForfaitaire = 1;
                    break;
                default:
                    # code...
                    break;
            } 

            return $CA_HT * $abbatementForfaitaire/100;
        }
    }

    public function getCotisationsSociales($CA_HT, $regimeActivite)
    {
        switch ($regimeActivite) {
            case 'BNC':
                $abbatementForfaitaire = 22;
                break;                
            case 'BIC':
                $abbatementForfaitaire = 12.8;
                break;
            case 'BIC Vente':
                $abbatementForfaitaire = 22;
                break;
            default:
                # code...
                break;
        } 

        return $CA_HT * $abbatementForfaitaire/100;
    }

    public function getCA_TTC($regimeFiscal, $regimeActivite, $CA_HT)
    {
        $cotiSociales = $this->getCotisationsSociales($regimeActivite, $CA_HT);
        
        if($regimeFiscal == 'Prélèvement à la source') 
        {
            return $CA_HT - $cotiSociales;
        }        
        
        else if($regimeFiscal == 'Prélèvement libératoire') 
        {
            $impotRevenu = $this->getImpotRevenu($regimeFiscal, $regimeActivite, $CA_HT);
            
            return $CA_HT - $cotiSociales - $impotRevenu;
        }
    }
    
}
/*

-------------------
IMPOT SUR LE REVENU
-------------------

- Si régime "prélèvement à la source" alors 
    Impot à payer = CA HT * (1 - % Abbatement forfaitaire)

    Abbatement forfaitaire : 
        BNC : 34%
        BIC : 50%
        BIC Vente : 71%

-> Le règlement de l'impôt sera prélevé ultérieument par le centre des impôts

- Si régime "prélèvement libératoire" alors
    Impôt à payer = CA HT * Abbatement forfaitaire/100
    
    Abbatement forfaitaire 2022 :
        BNC : 2.2%
        BIC : 1.7%
        BIC Vente : 1%

-> Prélevé directement par l'URSAFF

--------------------
COTISATIONS SOCIALES
--------------------

- Pour chaque régime 
    Cot. Soc. à payer = CA HT * Abbatement forfaitaire/100

    Abbatement forfaitaire 2022 :
        BNC : 22%
        BIC : 12.8%
        BIC Vente : 22%

-------------------
CALCUL CA TTC
-------------------

- Si régime "prélèvement à la source" alors 
    CA TTC = CA HT - cotisations sociales

- Si régime "prélèvement libératoire" alors
    CA TTC = CA HT - cotisations sociales - impôt sur le revenu
*/